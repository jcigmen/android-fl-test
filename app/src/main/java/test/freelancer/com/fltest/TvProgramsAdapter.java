package test.freelancer.com.fltest;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import test.freelancer.com.fltest.R;
import test.freelancer.com.fltest.model.TvProgram;

/**
 * @author Julious Igmen
 */
public class TvProgramsAdapter extends BaseAdapter {

    private List<TvProgram> programs;

    public TvProgramsAdapter(List<TvProgram> programs) {
        this.programs = programs;
    }

    @Override
    public int getCount() {
        return programs.size();
    }

    @Override
    public TvProgram getItem(int position) {
        return programs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewTag tag;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_tv_programs, parent, false);

            tag = new ViewTag();
            tag.nameTextView = (TextView) convertView.findViewById(R.id.adapter_tv_programs_name);
            tag.scheduleTextView = (TextView) convertView.findViewById(R.id.adapter_tv_programs_schedule);
            tag.channelTextView = (TextView) convertView.findViewById(R.id.adapter_tv_programs_channel);
            tag.ratingTextView = (TextView) convertView.findViewById(R.id.adapter_tv_programs_ratings);

            convertView.setTag(tag);
        } else {
            tag = (ViewTag) convertView.getTag();
        }

        TvProgram tvProgram = getItem(position);

        tag.nameTextView.setText(tvProgram.name);
        tag.scheduleTextView.setText(tvProgram.schedule.toString());
        tag.channelTextView.setText(tvProgram.channel);
        tag.ratingTextView.setText(convertView.getContext().getString(R.string.rated) + " " + tvProgram.rating);

        return convertView;
    }

    private class ViewTag {
        public TextView nameTextView;
        public TextView scheduleTextView;
        public TextView channelTextView;
        public TextView ratingTextView;
    }
}
