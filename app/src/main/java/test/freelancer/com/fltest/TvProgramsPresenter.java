package test.freelancer.com.fltest;

import android.widget.AbsListView;

import test.freelancer.com.fltest.listener.OnLoadTvProgramsListener;

/**
 * Stands as the adapter or bridge between the logic-less View and the Worker, which does the heavy-lifting.
 * The Presenter also receives the events that the View's components invoke, that which most of the time
 * utilizes the Worker.
 *
 * When loading the TV Programs, the first one to try will be through the API. When that fails, the app
 * will try reading through its local database storage if there are anything saved in there.
 *
 * @author Julious Igmen
 */
public interface TvProgramsPresenter extends OnLoadTvProgramsListener, AbsListView.OnScrollListener {

    /**
     * @param firstTime Whether it's the first time the TV Programs will be loaded
     */
    public void loadTvPrograms(boolean firstTime);
}
