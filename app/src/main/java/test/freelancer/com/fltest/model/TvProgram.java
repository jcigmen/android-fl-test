package test.freelancer.com.fltest.model;

/**
 * @author Julious Igmen
 */
public class TvProgram {

    public static class ProgramSchedule {

        public String startTime;
        public String endTime;

        public ProgramSchedule(String startTime, String endTime) {
            this.startTime = startTime;
            this.endTime = endTime;
        }

        @Override
        public String toString() {
            return startTime + " - " + endTime;
        }
    }

    public ProgramSchedule schedule;
    public String name;
    public String channel;
    public String rating;

    public TvProgram(String name, ProgramSchedule schedule, String channel, String rating) {
        this.name = name;
        this.schedule = schedule;
        this.channel = channel;
        this.rating = rating;
    }
}
