package test.freelancer.com.fltest;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import test.freelancer.com.fltest.async.LoadProgramsLocallyAsync;
import test.freelancer.com.fltest.async.SaveProgramsAsync;
import test.freelancer.com.fltest.listener.OnLoadTvProgramsListener;
import test.freelancer.com.fltest.model.TvProgram;
import test.freelancer.com.fltest.util.NetworkRequestWorker;
import test.freelancer.com.fltest.util.VolleyRequestWorkerImpl;

/**
 * @author Julious Igmen
 */
public class TvProgramsWorkerImpl implements TvProgramsWorker {

    private static final String TAG = TvProgramsWorkerImpl.class.getSimpleName();

    protected OnLoadTvProgramsListener listener;
    protected NetworkRequestWorker networkRequestWorker;
    protected Context context;

    public TvProgramsWorkerImpl(Context context, OnLoadTvProgramsListener listener) {
        this.context = context;
        this.listener = listener;

        networkRequestWorker = new VolleyRequestWorkerImpl(context, this);
    }

    @Override
    public void loadTvPrograms(int startCount) {
        // could be a global static, but for now let's keep it this way since there are no variations
        final String apiHome = "http://whatsbeef.net";
        final String apiGuide = "/wabz/guide.php?start=";

        String url = apiHome + apiGuide + startCount;

        networkRequestWorker.sendRequest(url, NetworkRequestWorker.GET);
    }

    @Override
    public void loadTvProgramsLocally() {
        LoadProgramsLocallyAsync loadProgramsLocallyAsync = new LoadProgramsLocallyAsync(this);
        loadProgramsLocallyAsync.execute();
    }

    @Override
    public void onNetworkRequestSuccess(String response) {
        List<TvProgram> programs = requestResponseToPrograms(response);

        boolean loadingFailed = programs == null;
        boolean noMoreProgramsToLoad = programs != null && programs.size() == 0;

        if (loadingFailed) {
            listener.onLoadTvProgramsOverApiFail(true);
        } else if (noMoreProgramsToLoad) {
            listener.onLoadTvProgramsOverApiFail(false);
        } else {
            savePrograms(programs);
            listener.onLoadTvProgramsSuccess(programs);
        }
    }

    @Override
    public void onLoadProgramsLocallySuccess(List<TvProgram> programs) {
        listener.onLoadTvProgramsLocallySuccess(programs);
    }

    @Override
    public void onLoadProgramsLocallyFail() {
        listener.onLoadTvProgramsLocallyFail();
    }

    private void savePrograms(List<TvProgram> programs) {
        SaveProgramsAsync saveProgramsAsync = new SaveProgramsAsync();
        saveProgramsAsync.execute(programs.toArray(new TvProgram[programs.size()]));
    }

    @Override
    public void onNetworkRequestFail(String error) {
        listener.onLoadTvProgramsOverApiFail(true);
    }

    /**
     * @param response the JSONObject (still in String) containing all the TV programs
     * @return null if there was an error parsing the request or zero-sized List if there are no longer items to load,
     * otherwise the loaded TV Programs
     */
    private List<TvProgram> requestResponseToPrograms(String response) {
        List<TvProgram> programs = new ArrayList<>();

        try {
            JSONObject rootObject = new JSONObject(response);

            // if there is no 'results' in the JSONObject, then there are no longer items to load,
            // thus we return the zero-sized list
            if (!rootObject.has("results")) {
                return programs;
            }

            JSONArray resultsArray = rootObject.getJSONArray("results");

            for (int i = 0; i < resultsArray.length(); i++) {
                JSONObject programObject = resultsArray.getJSONObject(i);

                String name = programObject.getString("name");
                String channel = programObject.getString("channel");
                String rating = programObject.getString("rating");
                String startTime = programObject.getString("start_time");
                String endTime = programObject.getString("end_time");
                TvProgram.ProgramSchedule schedule = new TvProgram.ProgramSchedule(startTime, endTime);

                TvProgram program = new TvProgram(name, schedule, channel, rating);

                programs.add(program);
            }

        } catch (JSONException e) {
            Log.e(TAG + "#requestResponseToPrograms", "Error parsing response: " + e.getMessage());
            return null; // if there's just on quirk with the response format, we tell NSA
        }

        return programs;
    }
}
