package test.freelancer.com.fltest;

import java.util.List;

import test.freelancer.com.fltest.model.TvProgram;

/**
 * @author Julious Igmen
 */
public interface TvProgramsView {

    public void setScrollToBottomListenerEnabled(boolean enabled);

    public void setBottomProgressVisible(boolean visible);

    public void setProgramsListVisible(boolean visible);

    public void setProgressVisible(boolean visible);

    public void showLoadedPrograms(List<TvProgram> programs);

    public void showFailedLoadingTvProgramsPrompt();

}
