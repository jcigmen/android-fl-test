package test.freelancer.com.fltest.async;

import android.database.SQLException;
import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;
import android.util.Log;

import test.freelancer.com.fltest.model.TvProgram;
import test.freelancer.com.fltest.model.TvProgramsTable;
import test.freelancer.com.fltest.util.DatabaseWorker;
import test.freelancer.com.fltest.util.DatabaseWorkerImpl;

/**
 * @author Julious Igmen
 */
public class SaveProgramsAsync extends AsyncTask<TvProgram, Void, Void> {

    private DatabaseWorker databaseWorker;

    public SaveProgramsAsync() {
        databaseWorker = DatabaseWorkerImpl.getInstance();
    }

    @Override
    protected Void doInBackground(TvProgram... programs) {
        for (TvProgram program : programs) {
            try {
                databaseWorker.startTransaction();

                String tableName = TvProgramsTable.TABLE_NAME;
                String[] columnNames = TvProgramsTable.getAllColumnNames();
                String preparedStatement = databaseWorker.createPreparedStatementQuery(tableName, columnNames, false);

                SQLiteStatement statement = databaseWorker.getDatabase().compileStatement(preparedStatement);
                statement.bindString(1, program.name);
                statement.bindString(2, program.schedule.startTime);
                statement.bindString(3, program.schedule.endTime);
                statement.bindString(4, program.channel);
                statement.bindString(5, program.rating);
                statement.execute();

                databaseWorker.endTransaction();
            } catch (SQLException e) {
                Log.e(SaveProgramsAsync.class.getSimpleName(), "Saving error: " + e.getMessage());
            }
        }

        return null;
    }
}
