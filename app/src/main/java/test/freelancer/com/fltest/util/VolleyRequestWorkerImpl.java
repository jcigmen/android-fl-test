package test.freelancer.com.fltest.util;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;

import test.freelancer.com.fltest.listener.OnNetworkRequestListener;

/**
 * @author Julious Igmen
 */
public class VolleyRequestWorkerImpl implements NetworkRequestWorker {

    private OnNetworkRequestListener listener;
    private Context context;

    public VolleyRequestWorkerImpl(Context context, OnNetworkRequestListener listener) {
        this.listener = listener;
        this.context = context;
    }

    /**
     * @param url
     * @param method Uses {@link Request.Method#POST} by default
     */
    @Override
    public void sendRequest(String url, int method) {
        int volleyMethod;

        switch (method) {
            case NetworkRequestWorker.GET:
                volleyMethod = Request.Method.GET;
                break;
            case NetworkRequestWorker.POST:
                volleyMethod = Request.Method.POST;
                break;
            case NetworkRequestWorker.DELETE:
                volleyMethod = Request.Method.DELETE;
                break;
            case NetworkRequestWorker.PUT:
                volleyMethod = Request.Method.PUT;
                break;
            default:
                volleyMethod = Request.Method.POST;
        }

        Response.Listener<String> successListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onNetworkRequestSuccess(response);
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String response;
                try {
                    response = new String(error.networkResponse.data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    response = "Unknown Error: failed parsing the network response.";
                } catch (NullPointerException e) {
                    response = "Unknown Error: probably no connection.";
                }

                listener.onNetworkRequestFail(response);
            }
        };

        StringRequest request = new StringRequest(volleyMethod, url, successListener, errorListener);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request);
    }
}
