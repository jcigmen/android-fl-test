package test.freelancer.com.fltest;

import android.content.Context;
import android.widget.AbsListView;

import java.util.List;

import test.freelancer.com.fltest.model.TvProgram;

/**
 * @author Julious Igmen
 */
public class TvProgramsPresenterImpl implements TvProgramsPresenter {

    private TvProgramsView view;
    private TvProgramsWorker worker;
    private List<TvProgram> programsList;
    private int currentStartCount = 0;

    public TvProgramsPresenterImpl(Context context, List<TvProgram> programsList, TvProgramsView view) {
        this.view = view;
        this.programsList = programsList;
        this.worker = new TvProgramsWorkerImpl(context, this);
    }

    @Override
    public void loadTvPrograms(boolean firstTime) {
        if (firstTime) {
            view.setProgramsListVisible(false);
            view.setProgressVisible(true);
        }

        worker.loadTvPrograms(currentStartCount);
    }

    @Override
    public void onLoadTvProgramsSuccess(List<TvProgram> programs) {
        currentStartCount += programs.size() >= 10 ? 10 : programs.size();

        programsList.addAll(programs);

        view.showLoadedPrograms(programs);
        view.setScrollToBottomListenerEnabled(true);
        view.setProgramsListVisible(true);
        view.setBottomProgressVisible(false);
        view.setProgressVisible(false);
    }

    @Override
    public void onLoadTvProgramsLocallySuccess(List<TvProgram> programs) {
        currentStartCount = 0;

        programsList.clear();
        programsList.addAll(programs);

        view.showLoadedPrograms(programs);
        view.setProgramsListVisible(true);
        view.setBottomProgressVisible(false);
        view.setProgressVisible(false);
    }

    @Override
    public void onLoadTvProgramsOverApiFail(boolean showPrompt) {
        view.setScrollToBottomListenerEnabled(false);

        // if there's no item in the programs list, try loading locally
        boolean viewHasNoVisiblePrograms = programsList.size() == 0;
        if (viewHasNoVisiblePrograms) {
            view.setProgramsListVisible(false);
            view.setBottomProgressVisible(false);
            view.setProgressVisible(true);
            worker.loadTvProgramsLocally();
        } else {
            view.setProgramsListVisible(true);
            view.setBottomProgressVisible(false);
            view.setProgressVisible(false);
        }
    }

    @Override
    public void onLoadTvProgramsLocallyFail() {
        view.showFailedLoadingTvProgramsPrompt();
        view.setScrollToBottomListenerEnabled(false);
        view.setProgramsListVisible(true);
        view.setBottomProgressVisible(false);
        view.setProgressVisible(false);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    /**
     * When the user scrolls through the list, we wait for the scroll to hit bottom, after then we
     * load more data.
     */
    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        boolean hasScroll = programsList.size() > 5;
        boolean scrolledToBotom = hasScroll && firstVisibleItem + visibleItemCount ==
                totalItemCount
                && totalItemCount != 0;

        if (scrolledToBotom) {
            view.setScrollToBottomListenerEnabled(false);
            view.setBottomProgressVisible(true);
            loadTvPrograms(false);
        } else {
            view.setBottomProgressVisible(false);
        }
    }
}
