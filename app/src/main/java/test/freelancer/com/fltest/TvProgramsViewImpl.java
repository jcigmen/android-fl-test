package test.freelancer.com.fltest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import test.freelancer.com.fltest.framework.BaseFragment;
import test.freelancer.com.fltest.model.TvProgram;

/**
 * Screen that displays the TV Programmes. The call to load the data is on {@link #onResume()}.
 * @author Julious Igmen
 */
public class TvProgramsViewImpl extends BaseFragment implements TvProgramsView {

    public static final String TAG = TvProgramsViewImpl.class.getSimpleName();

    private List<TvProgram> programsList;
    private ListView programsListView;
    private ProgressBar bottomProgresBar;
    private ProgressBar progressBar;
    private TvProgramsPresenter presenter;

    @Override
    public void onResume() {
        super.onResume();

        boolean shouldLoad = programsListView.getAdapter().getCount() == 0;

        if (shouldLoad) {
            presenter.loadTvPrograms(true);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        programsList = new ArrayList<>();

        presenter = new TvProgramsPresenterImpl(getActivity().getApplicationContext(), programsList, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tv_programs, container, false);

        progressBar = (ProgressBar) rootView.findViewById(R.id.tv_programs_progress);

        bottomProgresBar = (ProgressBar) rootView.findViewById(R.id.tv_programs_bottom_progress);
        setBottomProgressVisible(false);

        programsListView = (ListView) rootView.findViewById(R.id.tv_programs_list);
        programsListView.setAdapter(new TvProgramsAdapter(programsList));
        programsListView.setOnScrollListener(presenter);
        programsListView.setFastScrollEnabled(false);

        return rootView;
    }

    @Override
    public String getScreenTitle() {
        return getString(R.string.programme);
    }

    @Override
    public void setScrollToBottomListenerEnabled(boolean enabled) {
        if (enabled) {
            programsListView.setOnScrollListener(presenter);
        } else {
            programsListView.setOnScrollListener(null);
        }
    }

    @Override
    public void setBottomProgressVisible(boolean visible) {
        int flag = visible ? View.VISIBLE : View.GONE;
        bottomProgresBar.setVisibility(flag);
    }

    @Override
    public void setProgramsListVisible(boolean visible) {
        int flag = visible ? View.VISIBLE : View.GONE;
        programsListView.setVisibility(flag);
    }

    @Override
    public void setProgressVisible(boolean visible) {
        int flag = visible ? View.VISIBLE : View.GONE;
        progressBar.setVisibility(flag);
    }

    @Override
    public void showLoadedPrograms(List<TvProgram> programs) {
        programsListView.deferNotifyDataSetChanged();
    }

    @Override
    public void showFailedLoadingTvProgramsPrompt() {
        Toast.makeText(getActivity(), getString(R.string.failed_loading_programs), Toast.LENGTH_SHORT).show();
    }
}
