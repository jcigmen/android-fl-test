package test.freelancer.com.fltest.listener;

import java.util.List;

import test.freelancer.com.fltest.model.TvProgram;

/**
 * @author Julious Igmen
 */
public interface OnLoadTvProgramsListener {

    /**
     * When the programs are successfully loaded from the API
     */
    public void onLoadTvProgramsSuccess(List<TvProgram> programs);

    /**
     * When the programs are successfully loaded from local storage.
     */
    public void onLoadTvProgramsLocallySuccess(List<TvProgram> programs);

    public void onLoadTvProgramsOverApiFail(boolean showPrompt);

    public void onLoadTvProgramsLocallyFail();
}
