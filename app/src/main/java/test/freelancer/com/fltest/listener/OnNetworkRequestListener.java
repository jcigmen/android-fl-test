package test.freelancer.com.fltest.listener;

/**
 * @author Julious Igmen
 */
public interface OnNetworkRequestListener {

    public void onNetworkRequestSuccess(String response);

    public void onNetworkRequestFail(String error);
}
