package test.freelancer.com.fltest;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import test.freelancer.com.fltest.framework.ScreenController;
import test.freelancer.com.fltest.framework.ScreenControllerImpl;
import test.freelancer.com.fltest.framework.ScreenControllerProvider;
import test.freelancer.com.fltest.util.DatabaseWorkerImpl;

/**
 * @author Julious Igmen
 */
public class MainActivity extends ActionBarActivity implements ScreenControllerProvider {

    private ScreenController screenController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DatabaseWorkerImpl.init(getApplicationContext(), "test-fl.db", 1);

        setContentView(R.layout.activity_main);

        screenController = new ScreenControllerImpl(getSupportActionBar(), getSupportFragmentManager(), R.id.container);
        screenController.setToolbarVisible(false);

        showTvProgramsScreen();
    }

    private void showTvProgramsScreen() {
        screenController.navigateToScreen(new TvProgramsViewImpl(), TvProgramsViewImpl.TAG);
    }

    @Override
    public ScreenController getScreenController() {
        return screenController;
    }

}
