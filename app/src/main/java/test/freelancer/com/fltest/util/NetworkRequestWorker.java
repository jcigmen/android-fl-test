package test.freelancer.com.fltest.util;

import test.freelancer.com.fltest.listener.OnNetworkRequestListener;

/**
 * Describes how a network util class should be implemented.
 * By convention, all implementations must accept a {@link OnNetworkRequestListener} in their
 * constructor as a parameter.
 *
 * @author Julious Igmen
 */
public interface NetworkRequestWorker {

    public static final int POST = 0;
    public static final int GET = 1;
    public static final int PUT = 2;
    public static final int DELETE = 3;

    public void sendRequest(String url, int method);
}
