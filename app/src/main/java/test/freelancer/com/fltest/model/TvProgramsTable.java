package test.freelancer.com.fltest.model;

import java.util.HashMap;

/**
 * @author Julious Igmen
 */
public class TvProgramsTable {

    public static final String TABLE_NAME = "TvPrograms";
    public static final String PROGRAM_NAME = "name";
    public static final String START_TIME = "start_time";
    public static final String END_TIME = "end_time";
    public static final String CHANNEL = "channel";
    public static final String RATING = "rating";

    public static String[] getAllColumnNames() {
        return new String[] {
                PROGRAM_NAME,
                START_TIME,
                END_TIME,
                CHANNEL,
                RATING
        };
    }

    public static HashMap<String, String> getAllColumns() {
        HashMap<String, String> columns = new HashMap<String, String>();
        columns.put(PROGRAM_NAME, "TEXT PRIMARY KEY");
        columns.put(START_TIME, "TEXT");
        columns.put(END_TIME, "TEXT");
        columns.put(CHANNEL, "TEXT");
        columns.put(RATING, "TEXT");

        return columns;
    }

}
