package test.freelancer.com.fltest.framework;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

/**
 * @author Julious Igmen
 */
public abstract class BaseFragment extends Fragment implements FragmentManager.OnBackStackChangedListener {

    public abstract String getScreenTitle();

    @Override
    public void onResume() {
        super.onResume();

        changeActionBarTitle(getScreenTitle());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addBackstackListener();
    }

    private void addBackstackListener() {
        FragmentActivity activity = getActivity();
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(this);
    }

    @Override
    public void onBackStackChanged() {
        // when the user returns to this screen/fragment after pressing back, we return the title of the toolbar
        if (isVisible()) {
            changeActionBarTitle(getScreenTitle());
        }
    }

    private void changeActionBarTitle(String title) {
        getScreenController().setToolbarTitle(title);
    }

    public ScreenController getScreenController() {
        return ((ScreenControllerProvider) getActivity()).getScreenController();
    }
}
