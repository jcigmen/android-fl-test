package test.freelancer.com.fltest;

import test.freelancer.com.fltest.listener.OnLoadProgramsLocallyAsync;
import test.freelancer.com.fltest.listener.OnNetworkRequestListener;

/**
 * Mostly core logic; loads and saves the TV Programs.
 *
 * @author Julious Igmen
 */
public interface TvProgramsWorker extends OnLoadProgramsLocallyAsync, OnNetworkRequestListener {

    /**
     * Only a maximum of 10 results per request.
     * @param startCount the index of the result to start with (i.e., if result count is 28
     * and startCount is 0, the results 0-9 will be returned through the listener)
     */
    public void loadTvPrograms(int startCount);

    /**
     * Loads all the saved data in the local storage.
     */
    public void loadTvProgramsLocally();
}
