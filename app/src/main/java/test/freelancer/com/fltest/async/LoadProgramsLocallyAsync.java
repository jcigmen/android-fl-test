package test.freelancer.com.fltest.async;

import android.database.Cursor;
import android.database.SQLException;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import test.freelancer.com.fltest.listener.OnLoadProgramsLocallyAsync;
import test.freelancer.com.fltest.model.TvProgram;
import test.freelancer.com.fltest.model.TvProgramsTable;
import test.freelancer.com.fltest.util.DatabaseWorker;
import test.freelancer.com.fltest.util.DatabaseWorkerImpl;

/**
 * @author Julious Igmen
 */
public class LoadProgramsLocallyAsync extends AsyncTask<Void, Void, List<TvProgram>>{

    private DatabaseWorker databaseWorker;
    private OnLoadProgramsLocallyAsync listener;

    public LoadProgramsLocallyAsync(OnLoadProgramsLocallyAsync listener) {
        this.listener = listener;
        databaseWorker = DatabaseWorkerImpl.getInstance();
    }

    @Override
    protected List<TvProgram> doInBackground(Void... params) {
        List<TvProgram> programs = new ArrayList<>();
        String query = "SELECT * FROM " + TvProgramsTable.TABLE_NAME;

        try {
            Cursor cursor = databaseWorker.getDatabase().rawQuery(query, null);
            cursor.moveToNext();

            for (int i = 0; i < cursor.getCount(); i++) {
                String name = cursor.getString(cursor.getColumnIndex(TvProgramsTable.PROGRAM_NAME));
                String startTime = cursor.getString(cursor.getColumnIndex(TvProgramsTable.START_TIME));
                String endTime = cursor.getString(cursor.getColumnIndex(TvProgramsTable.END_TIME));
                String channel = cursor.getString(cursor.getColumnIndex(TvProgramsTable.CHANNEL));
                String rating = cursor.getString(cursor.getColumnIndex(TvProgramsTable.RATING));
                TvProgram.ProgramSchedule schedule = new TvProgram.ProgramSchedule(startTime, endTime);

                programs.add(new TvProgram(name, schedule, channel, rating));

                cursor.moveToNext();
            }

            cursor.close();
        } catch (SQLException e) {
            return null;
        }
        return programs;
    }

    @Override
    protected void onPostExecute(List<TvProgram> tvPrograms) {
        super.onPostExecute(tvPrograms);

        boolean loadingSuccess = tvPrograms != null;
        if (loadingSuccess) {
            listener.onLoadProgramsLocallySuccess(tvPrograms);
        } else {
            listener.onLoadProgramsLocallyFail();
        }
    }
}
