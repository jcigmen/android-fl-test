package test.freelancer.com.fltest.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;
import java.util.Map.Entry;

import test.freelancer.com.fltest.model.TvProgram;
import test.freelancer.com.fltest.model.TvProgramsTable;

/**
 * Utility class that uses a singleton for all operations that is about the database.
 * Note that {@link #init(Context, String, int)} must must invoked at the start of the app.
 *
 * @author Julious Igmen
 */
public class DatabaseWorkerImpl extends SQLiteOpenHelper implements DatabaseWorker {

    private static DatabaseWorkerImpl singletonInstance;

    private SQLiteDatabase database;

    public static void destroy() {
        if (singletonInstance != null && singletonInstance.database != null) {
            singletonInstance.database.close();
        }
        singletonInstance = null;
    }

    public static DatabaseWorkerImpl getInstance() {
        return singletonInstance;
    }

    /**
     * This must be invoked on app start once.
     */
    public static void init(Context context, String databaseName, int databaseVersion) {
        singletonInstance = new DatabaseWorkerImpl(context, databaseName, databaseVersion);
    }

    /**
     * Singleton - no other classes can make an instance of this class except itself.
     */
    private DatabaseWorkerImpl(Context context, String name, int version) {
        super(context, name, null, version);
    }

    /**
     * Creates an <code>INSERT OR REPLACE INTO</code> query string.
     * Considers that the first column is the ID.
     */
    public String createPreparedStatementQuery(String tableName, String[] columnNames, boolean idAutoIncrement) {
        String query = "INSERT OR REPLACE INTO " + tableName + " ( ";
        String valuesClause = " VALUES ( ";

        int startingIndex = idAutoIncrement ? 1 : 0;

        if (startingIndex == 1 && columnNames.length <= 1) {
            startingIndex = 0;
        }

        for (int i = startingIndex; i < columnNames.length; i++) {
            String column = columnNames[i];
            boolean notLastItem = i != columnNames.length - 1;

            query += column;
            valuesClause += "? ";

            if (notLastItem) {
                query += ", ";
                valuesClause += ", ";
            }
        }
        valuesClause += ")";
        query += ") " + valuesClause;

        return query;
    }

    private void createTable(String tableName, HashMap<String, String> columns) {
        String query = "CREATE TABLE " + tableName + " (";

        // add the columns
        for (Entry<String, String> entry : columns.entrySet())
            query += entry.getKey() + " " + entry.getValue() + ", ";

        // remove the extra comma in the end
        query = query.substring(0, query.length() - 2);

        query += ")";

        database.execSQL(query);
    }

    public void endTransaction() {
        if (database != null && database.isOpen()) {
            database.setTransactionSuccessful();
            database.endTransaction();
        }
    }

    public SQLiteDatabase getDatabase() {
        if (database == null) {
            database = getWritableDatabase();
        }
        return database;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        this.database = database;

        createTable(TvProgramsTable.TABLE_NAME, TvProgramsTable.getAllColumns());
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int oldVersion, int newVersion) {
    }

    public void startTransaction() {
        getDatabase().beginTransaction();
    }

}
