package test.freelancer.com.fltest.util;

import android.database.sqlite.SQLiteDatabase;

/**
 * @author Julious Igmen
 */
public interface DatabaseWorker {

    public void endTransaction();

    public void startTransaction();

    public String createPreparedStatementQuery(String tableName, String[] columnNames, boolean idAutoIncrement);

    public SQLiteDatabase getDatabase();
}
