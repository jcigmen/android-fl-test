package test.freelancer.com.fltest.framework;

/**
 * @author Julious Igmen
 */
public interface ScreenControllerProvider {

    public ScreenController getScreenController();
}
