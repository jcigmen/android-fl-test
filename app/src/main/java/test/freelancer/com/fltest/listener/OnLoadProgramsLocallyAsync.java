package test.freelancer.com.fltest.listener;

import java.util.List;

import test.freelancer.com.fltest.model.TvProgram;

/**
 * @author Julious Igmen
 */
public interface OnLoadProgramsLocallyAsync {

    public void onLoadProgramsLocallySuccess(List<TvProgram> programs);

    public void onLoadProgramsLocallyFail();
}
