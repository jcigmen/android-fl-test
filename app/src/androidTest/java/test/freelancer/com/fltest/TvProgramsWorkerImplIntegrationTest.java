package test.freelancer.com.fltest;

import android.test.ActivityInstrumentationTestCase2;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import test.freelancer.com.fltest.listener.OnLoadTvProgramsListener;
import test.freelancer.com.fltest.model.TvProgram;

/**
 * @author Julious Igmen
 */
public class TvProgramsWorkerImplIntegrationTest
        extends ActivityInstrumentationTestCase2<MainActivity> {

    private TvProgramsWorkerImpl worker;

    public TvProgramsWorkerImplIntegrationTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        // we're highly likely to just mock the listener on the fly so we'll make it null for now
        worker = new TvProgramsWorkerImpl(getActivity(), null);
    }

    public void testLoadTvPrograms() throws InterruptedException {
        assertCanLoadAnything();
    }

    private void assertCanLoadAnything() throws InterruptedException {
        final AtomicBoolean requestNotFinished = new AtomicBoolean(true);
        worker.listener = new OnLoadTvProgramsListener() {
            @Override
            public void onLoadTvProgramsSuccess(List<TvProgram> programs) {
                requestNotFinished.set(false);
            }

            @Override
            public void onLoadTvProgramsOverApiFail(boolean showPrompt) {
                requestNotFinished.set(false);
                fail();
            }

            @Override
            public void onLoadTvProgramsLocallyFail() {
                requestNotFinished.set(false);
                fail();
            }
        };

        worker.loadTvPrograms(0);

        synchronized (this) {
            while (requestNotFinished.get()) {
                wait(500);
            }
        }
    }
}
